# require 'byebug'

class Hangman
  GUESS_LIMIT = 8

  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = board
  end

  def play
    setup
    i = 0
    while i < GUESS_LIMIT
      take_turn

      board.each do |ch|
        if ch.nil?
          print '-'
        else
          print ch
        end
      end
      puts

      if won?
        puts "The Guesser wins!"
        return
      end

      i += 1
    end
    puts "The word was #{referee.get_word}"
  end

  def setup
    secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_length)
    @board = [nil] * secret_length
  end

  def take_turn
    guess = @guesser.guess(@board)
    matches = @referee.check_guess(guess)
    update_board(matches, guess)
    @guesser.handle_response(guess, matches)
  end

  def update_board(matches, guess)
    matches.each do |m|
      board[m] = guess
    end
  end

  def won?
    board.include?(nil) ? false : true
  end

end


class HumanPlayer
  attr_accessor :secret_length

  def initialize
    @secret_length = nil
  end

  def pick_secret_word
    puts "Please think of a secret word.."
    puts "Please enter the length of that word:"
    length = gets.chomp
    length.to_i
  end

  def register_secret_length(length)
    @secret_length = length
  end

  def guess(board)
    puts "Please guess a letter: "
    guess = gets.chomp
    guess
  end

  def check_guess(guess)
    puts "Please enter indices of your word equal to '#{guess}', comma separated:"
    match_indices = []
    gets.chomp.split(',').each do |i|
      match_indices.push(i.to_i)
    end
    match_indices
  end

  def handle_response(guess, response)
    if response.empty?
      puts "#{guess} not present in word."
    else
      puts "'#{guess}' found at indices: #{response}"
    end
  end

  def get_word
    puts "Computer couldn't guess it!  What was your word?"
    word = gets.chomp
    word
  end
end


class ComputerPlayer
  attr_accessor :dictionary, :secret_word, :guess, :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    secret_word.length
  end

  def check_guess(letter)
    match_indices = []
    secret_word.chars.each_with_index do |ch, i|
      if ch == letter
        match_indices.push(i)
      end
    end
    match_indices
  end

  # register secret length and intial filtering by length
  def register_secret_length(length)
    @candidate_words = @dictionary.reject { |w| w.length != length }
  end

  # frequency hash of eligible letters
  def letter_frequencies(board)
    # count letters at open positions
    letter_frequencies = Hash.new(0)
    @candidate_words.each do |word|
      board.each_with_index do |ch, i|
        if ch.nil?
          letter_frequencies[word[i]] += 1
        end
      end
    end
    letter_frequencies
  end

  # chooses the most frequent letter from letter_frequencies
  def guess(board)
    # debugger
    frequencies = letter_frequencies(board)
    guess = frequencies.sort_by { |ch, f| f }
    letter, _count = guess.last
    letter
  end

  # selects only the eligible candidate words for next guess
  def handle_response(guess, response)
    @candidate_words.select! do |word|
      keep = true
      word.chars.each_with_index do |ch, i|
        # guess was current letter, but response didn't include that index.
        if (ch == guess) && (!response.include?(i))
          keep = false
          break
        # guess wasn't current letter, but response said guess is at this index.
        elsif (ch != guess) && (response.include?(i))
          keep = false
          break
        end
      end
      keep
    end
  end

  def get_word
    @secret_word
  end

end


if __FILE__ == $PROGRAM_NAME
  dictionary = File.readlines('dictionary.txt').map(&:chomp)

  puts "Guesser (Comp: C, Human: H):"
  if gets.chomp == 'C'
    guesser = ComputerPlayer.new(dictionary)
  else
    guesser = HumanPlayer.new
  end

  puts "Referee (Comp: C, Human: H):"
  if gets.chomp == 'C'
    referee = ComputerPlayer.new(dictionary)
  else
    referee = HumanPlayer.new
  end

  # debugger
  Hangman.new({guesser: guesser, referee: referee}).play

end
